FROM nginx:1.19.6-alpine

COPY ./nginx/nginx.conf /etc/nginx/nginx.conf

COPY ./app/dist/app /usr/share/nginx/html

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
